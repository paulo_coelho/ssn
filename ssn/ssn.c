/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ssn.h"
#include "parson.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ssn_t
{
  int                total_users;
  struct ssn_user_t* users;
};

struct ssn_user_t
{
  int              seq_num;
  struct ssn_msg_t msgs[MAX_POSTS];
  int              followers[MAX_FOLLOWERS];
  int              msgs_count;
  int              followers_count;
  int              following_count;
  int              partition;
  int              followers_partitions[MAX_FOLLOWERS];
  int              followers_partitions_count;
};

/*
struct ssn_msg_t
{
  int  user_id;
  int  seq_num;
  char msg[MAX_MSG_SIZE];
};
*/

struct ssn_t*
ssn_new(int total_users)
{
  struct ssn_t* s = malloc(sizeof(struct ssn_t));
  s->users = malloc(total_users * sizeof(struct ssn_user_t));
  s->total_users = total_users;
  memset(s->users, 0, total_users * sizeof(struct ssn_user_t));
  return s;
}

struct ssn_t*
ssn_load_from_json_file(const char* file_path)
{
  JSON_Value*   root_value;
  JSON_Array *  users, *followers;
  JSON_Object * allUsers, *user;
  struct ssn_t* s = NULL;
  size_t        i, j;
  int           user_id;

  printf("SSN: Loading from file '%s'...", file_path);
  root_value = json_parse_file(file_path);

  if (json_value_get_type(root_value) != JSONObject) {
    printf("Error parsing file.\n");
    goto cleanup;
  }

  allUsers = json_value_get_object(root_value);
  users = json_object_get_array(allUsers, "allUsers");

  if (users == NULL) {
    printf("Error getting users list from file.\n");
    goto cleanup;
  }

  s = ssn_new(json_array_get_count(users));

  for (i = 0; i < json_array_get_count(users); i++) {
    user = json_array_get_object(users, i);
    user_id = (int)json_object_get_number(user, "userId");
    ssn_set_partition(s, user_id,
                      (int)json_object_get_number(user, "partitionId") - 1);
  }
  for (i = 0; i < json_array_get_count(users); i++) {
    user = json_array_get_object(users, i);
    user_id = (int)json_object_get_number(user, "userId");
    followers = json_object_get_array(user, "followers");

    for (j = 0; j < json_array_get_count(followers); j++) {
      ssn_follow(s, (int)json_array_get_number(followers, j), user_id);
    }
  }
  printf("...done!\n");
cleanup:
  json_value_free(root_value);
  return s;
}

void
ssn_free(struct ssn_t* s)
{
  if (s == NULL || s->total_users == 0)
    return;

  free(s->users);
  free(s);
}

static void
update_partitions(struct ssn_t* s, int follower, int followed)
{
  int i;
  for (i = 0; i < s->users[followed].followers_partitions_count; i++)
    if (s->users[followed].followers_partitions[i] ==
        s->users[follower].partition)
      return;

  s->users[followed]
    .followers_partitions[s->users[followed].followers_partitions_count++] =
    s->users[follower].partition;
}

void
ssn_follow(struct ssn_t* s, int follower, int followed)
{
  // valid users?
  if (follower >= s->total_users || followed >= s->total_users ||
      follower == followed)
    return;

  int i = 0, j = 0, size = s->users[followed].followers_count;

  // max followers/follows reached?
  if (size == MAX_FOLLOWERS)
    return;

  // follower follows followed
  for (i = 0; i < size; i++)
    if (s->users[followed].followers[i] == follower) {
      update_partitions(s, follower, followed);
      return;
    }

  for (i = 0; i < size; i++)
    if (s->users[followed].followers[i] > follower)
      break;

  for (j = size; j > i; j--)
    s->users[followed].followers[j] = s->users[followed].followers[j - 1];

  s->users[followed].followers[i] = follower;
  s->users[followed].followers_count++;
  s->users[follower].following_count++;

  update_partitions(s, follower, followed);
}

int
ssn_get_partition(struct ssn_t* s, int user)
{
  return s->users[user].partition;
}

int
ssn_get_follower_partitions(struct ssn_t* s, int user, int** partitions)
{
  *partitions = s->users[user].followers_partitions;
  return s->users[user].followers_partitions_count;
}

void
ssn_set_partition(struct ssn_t* s, int user, int partition)
{
  s->users[user].partition = partition;
}

int
ssn_get_users_count(struct ssn_t* s)
{
  return s->total_users;
}

int
ssn_get_followers(struct ssn_t* s, int user, int** followers)
{
  // valid user?
  if (user >= s->total_users) {
    *followers = NULL;
    return -1;
  }
  *followers = s->users[user].followers;
  return s->users[user].followers_count;
}

int
ssn_get_timeline(struct ssn_t* s, int user, struct ssn_msg_t** msgs)
{
  // valid user?
  if (user >= s->total_users) {
    *msgs = NULL;
    return -1;
  }

  *msgs = s->users[user].msgs;
  return s->users[user].msgs_count;
}

void
ssn_post(struct ssn_t* s, int user, const char* post, int post_size)
{
  // valid user? valid msg size?
  if (user >= s->total_users || post_size > MAX_MSG_SIZE)
    return;

  int fid = 0, msg_count = 0, i = 0;
  s->users[user].seq_num++;
  for (i = 0; i < s->users[user].followers_count; i++) {
    fid = s->users[user].followers[i];

    // if max posts reached, ignore post and restart from 0
    if (s->users[fid].msgs_count == MAX_POSTS)
      s->users[fid].msgs_count = 0;

    msg_count = s->users[fid].msgs_count;
    s->users[fid].msgs[msg_count].seq_num = s->users[user].seq_num;
    s->users[fid].msgs[msg_count].user_id = user;
    memcpy(s->users[fid].msgs[msg_count].msg, post, post_size);
    s->users[fid].msgs_count++;
  }
}

int
ssn_has_common_followers(struct ssn_t* s, int user1, int user2)
{
  struct ssn_user_t *u1 = &s->users[user1], *u2 = &s->users[user2];
  int                idx1 = 0, idx2 = 0, j = 0;

  if (user1 >= s->total_users || user2 >= s->total_users)
    return 0;

  for (; idx1 < u1->followers_count && idx2 < u2->followers_count; idx1++) {
    for (j = idx2;
         j < u2->followers_count && u2->followers[j] <= u1->followers[idx1];
         j++) {
      if (u1->followers[idx1] == u2->followers[j])
        return 1;
    }
    idx2 = j;
  }
  return 0;
}

static void
print_message(struct ssn_msg_t* msg)
{
  printf("\t--From user %d, seq. num %d, msgs '%s'\n", msg->user_id,
         msg->seq_num, msg->msg);
}

void
ssn_print(struct ssn_t* s)
{
  int i, j;
  for (i = 0; i < s->total_users; i++) {
    printf("\n----\nUSER %d - partition %d\n\tTimeline (%d messages):\n", i,
           s->users[i].partition, s->users[i].msgs_count);
    for (j = 0; j < s->users[i].msgs_count; j++) {
      print_message(&s->users[i].msgs[j]);
    }
    printf("\n\t%d followers:\n\t", s->users[i].followers_count);
    for (j = 0; j < s->users[i].followers_count; j++) {
      printf("%d(PART. %d) ", s->users[i].followers[j],
             ssn_get_partition(s, s->users[i].followers[j]));
    }
    printf("\n");
  }

  const int size = MAX_FOLLOWERS; // max number of partitions
  int       partitions[MAX_FOLLOWERS] = { 0 }, stats[MAX_FOLLOWERS] = { 0 },
      f_count[MAX_FOLLOWERS], part = 0, f_count_max = 0;

  printf("\n----\nMore statistics:\n");
  for (i = 0; i < s->total_users; i++) {
    j = s->users[i].partition;
    if (j > part)
      part = j;
    partitions[j]++;
  }

  for (i = 0; i <= part; i++) {
    printf("\nPartition %d with %d users.\n", i, partitions[i]);
    f_count_max = 0;
    memset(f_count, 0, MAX_FOLLOWERS * sizeof(int));
    for (j = 0; j < s->total_users; j++) {
      f_count_max = s->users[j].followers_count > f_count_max
                      ? s->users[j].followers_count
                      : f_count_max;
      if (s->users[j].partition == i)
        f_count[s->users[j].followers_count]++;
    }
    for (j = 0; j < f_count_max; j++) {
      if (f_count[j] > 0)
        printf(" - %4dU w/ %4dF - ", f_count[j], j);
      if (!(j % 5))
        printf("\n");
    }
    printf("\n");
  }

  for (i = 0; i < s->total_users; i++) {
    memset(partitions, 0, size * sizeof(int));
    for (j = 0; j < s->users[i].followers_count; j++) {
      partitions[s->users[s->users[i].followers[j]].partition]++;
    }

    part = partitions[s->users[i].partition] == 0 ? 1 : 0;
    for (j = 0; j < size; j++) {
      if (partitions[j] > 0)
        part++;
    }
    stats[part]++;
  }

  for (i = 0; i < size; i++) {
    if (stats[i] > 0)
      printf("%5d/%d users with followers from %d partition(s).\n", stats[i],
             s->total_users, i);
  }
}
