/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SSN_SSN_H
#define SSN_SSN_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_MSG_SIZE 64
#define MAX_FOLLOWERS 200
#define MAX_POSTS 10

struct ssn_t;
struct ssn_user_t;

struct ssn_msg_t
{
  int  user_id;
  int  seq_num;
  char msg[MAX_MSG_SIZE];
};

struct ssn_t* ssn_new(int total_users);
struct ssn_t* ssn_load_from_json_file(const char* file_path);
void ssn_free(struct ssn_t* ssn);

int ssn_get_partition(struct ssn_t* s, int user);
int ssn_get_follower_partitions(struct ssn_t* s, int user, int** partitions);
void ssn_set_partition(struct ssn_t* s, int user, int partition);
int ssn_get_users_count(struct ssn_t* s);
int ssn_get_followers(struct ssn_t* s, int user, int** followers);
int ssn_has_common_followers(struct ssn_t* s, int user1, int user2);

void ssn_follow(struct ssn_t* s, int follower, int followed);
int ssn_get_timeline(struct ssn_t* ssn, int user, struct ssn_msg_t** msgs);
void ssn_post(struct ssn_t* ssn, int user, const char* post, int post_size);

void ssn_print(struct ssn_t* ssn);

#ifdef __cplusplus
}
#endif

#endif // SSN_SSN_H
