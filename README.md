# SSN: Simple social network #
Minimalistic social network implementation for use in academic experiments.
Basic operations include: post, getTimeline, follow, unfollow.
Can load the social network graph from JSON files.

# Building
```
#!bash
git clone https://paulo_coelho@bitbucket.org/paulo_coelho/ssn.git
mkdir ssn/build
cd ssn/build
cmake ..
make
```

# Running
Examples are available in the ```sample``` folder.

All rights reserved to [USI][1]

[1]: https://inf.usi.ch