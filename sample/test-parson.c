/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "parson.h"
#include <stdio.h>

int
main(int argc, char** argv)
{
  JSON_Value*  root_value;
  JSON_Array * users, *followers;
  JSON_Object *allUsers, *user;
  size_t       i, j;

  printf("checking file '%s'\n", argv[1]);

  /* parsing json and validating output */
  root_value = json_parse_file(argv[1]);
  if (json_value_get_type(root_value) != JSONObject) {
    printf("error parsing file: exiting.\n");
    return 1;
  }

  /* getting array from root value and printing user info */
  allUsers = json_value_get_object(root_value);

  users = json_object_get_array(allUsers, "allUsers");
  // printf("root = %s\n", json_object_get_string(allUsers, "allUsers"));
  printf("%-10.10s %-10.10s %s\n", "userId", "partitionId", "Followers[]");
  for (i = 0; i < json_array_get_count(users); i++) {
    user = json_array_get_object(users, i);
    printf("%5.0lf %10.0lf      [", json_object_get_number(user, "userId"),
           json_object_get_number(user, "partitionId"));
    followers = json_object_get_array(user, "followers");
    for (j = 0; j < json_array_get_count(followers); j++) {
      printf(" %3.0lf ", json_array_get_number(followers, j));
    }
    printf("]\n");
  }

  /* cleanup code */
  json_value_free(root_value);

  return 0;
}