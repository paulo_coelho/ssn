/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ssn.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TOTAL_POSTS 1000
#define MAX_PARTITIONS 100

int
main(int argc, char** argv)
{
  struct ssn_t*     s = NULL;
  char              post[MAX_MSG_SIZE];
  int               count = 0, i = 0, j = 0, k = 0, *fp = NULL;
  struct ssn_msg_t* msgs;

  if (argc != 2) {
    printf("Usage: %s <user-file.json>", argv[0]);
    return 1;
  }

  srand(time(NULL));
  s = ssn_load_from_json_file(argv[1]);
  count = ssn_get_users_count(s);

  for (i = 0; i < TOTAL_POSTS; i++) {
    sprintf(post, "post number %d", (i + 1));
    ssn_post(s, rand() % count, post, strlen(post));
  }

  for (i = 0; i < count; i++) {
    printf("\nTIMELINE FOR USER %d:\n", i);
    for (j = 0; j < ssn_get_timeline(s, i, &msgs); j++) {
      printf("\tuser %d, sn %d, msg '%s'\n", msgs[j].user_id, msgs[j].seq_num,
             msgs[j].msg);
    }
  }

  ssn_print(s);

  printf("Users 1 and 2 have followers in common? %d\n",
         ssn_has_common_followers(s, 1, 2));
  printf("Users 2 and 1 have followers in common? %d\n",
         ssn_has_common_followers(s, 2, 1));

  for (i = 0; i < count; i++) {
    k = ssn_get_follower_partitions(s, i, &fp);
    printf("\nUser %d has followers from %d partitions: ", i, k);
    for (j = 0; j < k; j++)
      printf("%3d ", fp[j]);
  }

  ssn_free(s);
  return 0;
}